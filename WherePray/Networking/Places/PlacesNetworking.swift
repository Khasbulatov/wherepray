//
//  PlacesNetworking.swift
//  WherePray
//
//  Created by dev717 on 07.11.2020.
//

import Foundation
import Alamofire

enum PlacesNetworking {
    case getPlaces(skip: Int)
    case createPlaces(prayItem: AddPray)
}

extension PlacesNetworking: TargetType {
    var baseURL: String {
        switch self {
        default:
            return "http://iosdev-001-site1.itempurl.com/"
        }
    }
    
    var path: String {
        switch self {
        case .getPlaces(let skip):
            return "place?take=20&skip=\(skip)"
        case .createPlaces:
            return "place"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getPlaces:
            return .get
        case .createPlaces:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .getPlaces:
            return .requestPlain
        case .createPlaces(let prayItem):
            let enc = try? JSONEncoder().encode(prayItem)

            if let enc = enc, let dictionary = try? JSONSerialization.jsonObject(with: enc, options: .allowFragments) as? [String: Any]  {
                print("add pray dic = ", dictionary)
                return .requestParameters(parameters: dictionary, encoding: URLEncoding.default)
            }
            return .requestParameters(parameters: [:], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return [:]
        }
    }
}
