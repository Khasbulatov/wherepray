//
//  PlacesAPI.swift
//  WherePray
//
//  Created by dev717 on 07.11.2020.
//

import UIKit
import Alamofire

protocol PlacesAPIProtocol {
    func getPlaces(skip: Int, completion: @escaping (Result<BaseResponse<[PrayModel]>?, NSError>) -> Void)
    
    func sendPlace(prayItem: AddPray, completion: @escaping (Result<BaseResponse<Bool>?, NSError>) -> Void)

}

class PlacesAPI: BaseAPI<PlacesNetworking>, PlacesAPIProtocol {
    func sendPlace(prayItem: AddPray, completion: @escaping (Result<BaseResponse<Bool>?, NSError>) -> Void) {
        
        self.fetchData(target: .createPlaces(prayItem: prayItem), responseClass: BaseResponse<Bool>.self) { (result) in
            completion(result)
        }
    }
    
    
    //MARK:- Requests
    
    func getPlaces(skip: Int, completion: @escaping (Result<BaseResponse<[PrayModel]>?, NSError>) -> Void) {
        
        self.fetchData(target: .getPlaces(skip: skip), responseClass: BaseResponse<[PrayModel]>.self) { (result) in
            completion(result)
        }
        
    }
    
    func addPray(prayItem: AddPray, and img: UIImage, completion: @escaping (Result<BaseResponse<Bool>?, NSError>) -> Void) {
        let url = "http://dev717-001-site1.btempurl.com/place"
        
        print("url = \(url)")
        
        let enc = try? JSONEncoder().encode(prayItem)

        let imageData = img.jpegData(compressionQuality: 1)
        print(img, imageData!)
        
        if let enc = enc, let param = try? JSONSerialization.jsonObject(with: enc, options: .allowFragments) as? [String: Any]  {
            print("add pray dic = ", param)
            
            AF.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(imageData!, withName: "Files" , fileName: "file.jpeg", mimeType: "image/jpeg")
                    for (key, value) in param {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                    }
            },
                to: url, method: .post, headers: nil)
                .response { resp in
                    print("resp = ", resp)
            }
        }
    }
}
