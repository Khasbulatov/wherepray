//
//  Protocols.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

protocol HomeControllerDelegate: class {
    func handleMenuToggle(forMenuOption menuOption: MenuItem?)
}
