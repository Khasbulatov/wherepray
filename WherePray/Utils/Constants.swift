//
//  Constants.swift
//  WherePray
//
//  Created by dev717 on 07.11.2020.
//

import Foundation


struct ErrorMessage {
    static let genericError = "Something went wrong please try again later"
}
