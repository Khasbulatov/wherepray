//
//  MenuItem.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

struct MenuItem {
    enum TypeMenu {
        case logo, prayList, infoApp, rate, callBack
    }
    var type: TypeMenu
    var title: String?
    var img: String?
}

