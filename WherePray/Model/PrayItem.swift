//
//  PrayItem.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import Foundation
import MapKit

struct Locations: Codable {
    var locations: [PrayItems]
}

class PrayItems: MKPointAnnotation, Codable {
    var header: String {
        didSet {
            title = header
        }
    }
    var adress: String
    var img: String?
    var isOpen: Bool
    var latitude: Double
    var longitude: Double
    
    override init() {
        header = ""
        adress = ""
        isOpen = false
        latitude = 0
        longitude = 0
        super.init()
    }
    
    static func loadJson() -> [PrayItems]?{
        let decoder = JSONDecoder()
        if let path = Bundle.main.path(forResource: "PrayItems", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let locations = try? decoder.decode(Locations.self, from: data)
                return locations?.locations
            } catch {
                print("erroir")
                return nil
            }
        }
        return nil
    }
}

class PrayModel: Codable {
    var id: Int
    var title: String?
    var latitude: Double?
    var longitude: Double?
    var description: String?
    var publishDate: String?
    var adress: String?
    var imageLinks: [String]?
}

struct AddPray: Codable {
    var title: String
    var latitude: Double
    var longitude: Double
    var description: String
    var adress: String
}
