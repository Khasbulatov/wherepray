//
//  TabBarViewController.swift
//  WherePray
//
//  Created by Kerim Khasbulatov on 03.01.2021.
//

import UIKit

final class TabBarViewController: UITabBarController {
    //MARK: VARIABLE
    
    private var bounceAnimation: CAKeyframeAnimation = {
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0, 1.3, 0.9, 1.0]
        bounceAnimation.duration = TimeInterval(0.3)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        return bounceAnimation
    }()
    
    var isProfile = false
    lazy public var prayListController: PrayListController = {
        
        let prayListController = PrayListController.create(viewModel: PrayListViewModel())
        
        let defaultImage = UIImage(named: "list")!.withRenderingMode(.alwaysTemplate)
        let tabBarItems = (title: title, image: defaultImage)
        
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: nil)
        
        prayListController.tabBarItem = tabBarItem
        
        return prayListController
    }()
    
    lazy public var mapViewController: MapViewController = {
        
        let mapViewController = MapViewController.create(viewModel: MapVCViewModel())
        
        let defaultImage = UIImage(named: "map")!.withRenderingMode(.alwaysTemplate)
        let tabBarItems = (title: title, image: defaultImage)
        
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: nil)
        
        mapViewController.tabBarItem = tabBarItem
        
        return mapViewController
    }()
    
    lazy public var settingController: SettingController = {
        
        let profileVC = SettingController()
        
        let defaultImage = UIImage(named: "tune")!.withRenderingMode(.alwaysTemplate)
        let tabBarItems = (title: title, image: defaultImage)
        
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: nil)
        
        profileVC.tabBarItem = tabBarItem
        
        return profileVC
    }()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0.5294117647, green: 0.5294117647, blue: 0.5294117647, alpha: 1)
        tabBar.clipsToBounds = true
        
        tabBar.layer.borderWidth = 0.50
        tabBar.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        tabBar.clipsToBounds = true
    }
}

//MARK: ACTIONS
extension TabBarViewController {
    func addViewControllers() {
        let prayListNC = UINavigationController(rootViewController: prayListController)
        let settingNC = UINavigationController(rootViewController: settingController)
                
        self.viewControllers = [mapViewController, prayListNC, settingNC]
        
        for vc in self.viewControllers! {
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        }
    }
}

extension TabBarViewController: UITabBarControllerDelegate  {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        var tabBarView: [UIView] = []
//
//        for i in tabBar.subviews {
//            if i.isKind(of: NSClassFromString("UITabBarButton")! ) {
//                tabBarView.append(i)
//            }
//        }
//
//        if !tabBarView.isEmpty {
//            UIView.animate(withDuration: 0.15, animations: {
//                tabBarView[item.tag].transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
//            }, completion: { _ in
//                UIView.animate(withDuration: 0.15) {
//                    tabBarView[item.tag].transform = CGAffineTransform.identity
//                }
//            })
        //        }
        
        guard let idx = tabBar.items?.firstIndex(of: item),
              tabBar.subviews.count > idx + 1,
              let imageView = tabBar.subviews[idx + 1].subviews.compactMap({ $0 as? UIImageView }).first else {
            return
        }
        
        imageView.layer.add(bounceAnimation, forKey: nil)
    }
}

@objc(LNColoredHairlineTabBar)
public class ColoredHairlineTabBar: UITabBar {
    private var hairline : UIView?
    
    public var hairlineColor : UIColor = UIColor.red {
        didSet {
            hairline?.backgroundColor = hairlineColor;
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if hairline == nil {
            hairline = UIView()
            hairline?.backgroundColor = hairlineColor
            addSubview(hairline!)
        }
        
        hairline?.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 1 / UIScreen.main.scale)
        
        //Hide the original hairline.
        subviews.filter({ $0 is UIImageView }).forEach({ ($0 as! UIImageView).isHidden = true })
    }
}

