//
//  CardViewController.swift
//  WherePray
//
//  Created by dev717 on 25.10.2020.
//

import UIKit
import MapKit

class CardViewController: UIViewController {
    private var didSetupConstraints = false
    
    var handler: ()->Void = {}
    
    //headerView
    var handleArea: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.4431372549, blue: 0.4745098039, alpha: 1)
        return view
    }()
    
    var mainView: UIView = {
        let view = UIView()
        return view
    }()
    
    var cancelBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("×", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = btn.titleLabel?.font.withSize(40)
        btn.addTarget(self, action: #selector(closeCard), for: .touchUpInside)
        return btn
    }()
    
    //DetailView
    let imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.clipsToBounds = true
        imgView.contentMode = .scaleAspectFill
        imgView.layer.cornerRadius = 4
        return imgView
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.boldSystemFont(ofSize: 17)
        lbl.textColor = .black
        return lbl
    }()
    
    let adressLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.numberOfLines = 2
        lbl.textColor = .black
        return lbl
    }()
    
    let openCloseLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        return lbl
    }()
    
    let routeBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Проложить маршрут", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = btn.titleLabel?.font.withSize(17)
        btn.backgroundColor = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
        btn.addTarget(self, action: #selector(routeAction), for: .touchUpInside)
        btn.layer.cornerRadius = 10
        return btn
    }()
    
    private var location: CLLocation?
    var startFrame: CGRect?
    
    init(startFrame: CGRect? = nil) {
        self.startFrame = startFrame
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modalPresentationStyle = .custom
        if startFrame != nil {
            self.transitioningDelegate = self
        }
                
        self.view.backgroundColor = .white
        setUI()
    }
}

//MARK: - Set UI
extension CardViewController {
    func configure(prayItem: PrayItems) {
        location = CLLocation(latitude: prayItem.latitude, longitude: prayItem.longitude)
        titleLbl.text = prayItem.header
        adressLbl.text = prayItem.adress
        if let img = prayItem.img {
            imgView.sd_setImage(with: URL(string: img), completed: nil)
        }
        if prayItem.isOpen {
            openCloseLbl.text = "Открыто"
            openCloseLbl.textColor = .systemGreen
        } else {
            openCloseLbl.text = "Закрыто"
            openCloseLbl.textColor = .systemRed
        }
    }
    
    func configure(prayItem: PrayModel) {
        if let ltd = prayItem.latitude, let lng = prayItem.longitude {
            location = CLLocation(latitude: ltd, longitude: lng)
        }
        titleLbl.text = prayItem.title
        adressLbl.text = prayItem.adress
        if let img = prayItem.imageLinks?.first {
            imgView.sd_setImage(with: URL(string: img), completed: nil)
        }
    }
    
    fileprivate
    func setUI() {
        handleArea.addSubview(lineView)
        handleArea.addSubview(cancelBtn)
        view.addSubview(handleArea)
        mainView.addSubview(imgView)
        mainView.addSubview(titleLbl)
        mainView.addSubview(adressLbl)
        mainView.addSubview(openCloseLbl)
        mainView.addSubview(routeBtn)
        view.addSubview(mainView)
        
        if startFrame != nil {
            lineView.isHidden = true
        } else {
            lineView.isHidden = false
        }
        
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            let topConstrForHandleArea = startFrame != nil ? 20 : -20
            handleArea.snp.makeConstraints { make in
                make.width.equalToSuperview()
                make.centerX.equalTo(view)
                make.height.equalTo(65)
                make.top.equalToSuperview().offset(topConstrForHandleArea)
            }
            
            lineView.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.height.equalTo(7)
                make.width.equalTo(61)
            }
            
            cancelBtn.snp.makeConstraints { (make) in
                make.centerY.equalTo(handleArea).offset(10)
                make.right.equalTo(handleArea.snp.right).inset(20)
                make.height.width.equalTo(40)
            }
            
            mainView.snp.makeConstraints { (make) in
                make.top.equalTo(handleArea.snp.bottom)
                make.left.right.bottom.equalToSuperview()
            }
            
            titleLbl.snp.makeConstraints { (make) in
                make.top.equalToSuperview()
                make.width.equalToSuperview()
                make.leading.trailing.equalTo(20)
            }
            
            adressLbl.snp.makeConstraints { (make) in
                make.top.equalTo(titleLbl.snp.bottom).offset(8)
                make.left.equalTo(titleLbl.snp.left)
                make.width.lessThanOrEqualTo(220)
            }
            
            routeBtn.snp.makeConstraints { (make) in
                make.left.right.equalToSuperview().inset(20)
                make.height.equalTo(45)
                make.top.equalTo(adressLbl.snp.bottom).offset(20)
            }
            
            openCloseLbl.snp.makeConstraints { (make) in
                make.trailing.equalTo(routeBtn.snp.trailing)
                make.centerY.equalTo(adressLbl)
            }
            
            imgView.snp.makeConstraints { (make) in
                make.left.right.equalToSuperview().inset(20)
                make.height.equalTo(135)
                make.top.equalTo(routeBtn.snp.bottom).offset(20)
            }
            
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
}

//MARK: - Set Actions
extension CardViewController {
    @objc
    fileprivate
    func closeCard() {
        if startFrame != nil {
            dismiss(animated: true, completion: nil)
        } else {
            handler()
        }
    }
    
    @objc
    fileprivate
    func routeAction() {
        print("route action")
        guard let location = location,
              let url = URL(string: "http://maps.apple.com/maps?saddr=&daddr=\(location.coordinate.latitude),\(location.coordinate.longitude)")
        else {
            return
        }
        
        UIApplication.shared.open(url)
    }
}

extension CardViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimatorPresent(startFrame: self.startFrame!)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimatorDismiss(endFrame: self.startFrame!)
    }
}
