//
//  ContainerMapVC.swift
//  WherePray
//
//  Created by dev717 on 15.11.2020.
//

import UIKit
import MapKit

class ContainerMapVC: UIViewController {
    private var didSetupConstraints = false
    var handler: (()->Void)?
    var setCenterPosition: ((CLLocationCoordinate2D?)->Void)?
    private var centerPosition: CLLocationCoordinate2D?
    
    fileprivate let locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Внести изменения"
        lbl.font = UIFont.boldSystemFont(ofSize: 18)
        lbl.contentMode = .center
        lbl.textAlignment = .center
        return lbl
    }()
    
    let mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.layer.cornerRadius = 5
        mapView.showsCompass = false
        mapView.showsScale = true
        let initialLocation = CLLocation(latitude: 42.582935, longitude: 47.847174)
        mapView.centerToLocation(initialLocation)
        return mapView
    }()
    
    let saveBtn: UIButton = {
        let btn = UIButton()
        btn.layer.cornerRadius = 5
        btn.setTitle("Сохранить", for: .normal)
        btn.backgroundColor = .white
        btn.setTitleColor(#colorLiteral(red: 0.3215686275, green: 0.5019607843, blue: 0.7058823529, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        return btn
    }()
    
    let lineBetweenBtns: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    let cancelBtn: UIButton = {
        let btn = UIButton()
        btn.layer.cornerRadius = 5
        btn.setTitle("Отмена", for: .normal)
        btn.backgroundColor = .white
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.setTitleColor(#colorLiteral(red: 0.3215686275, green: 0.5019607843, blue: 0.7058823529, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        return btn
    }()
    
    private var currentLocationBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(named: "userLocation"), for: .normal)
        btn.addTarget(self, action: #selector(tapCurrentLocation), for: .touchUpInside)
        return btn
    }()
    
    private var plusBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("➕", for: .normal)
        let font = UIFont.systemFont(ofSize: 18)
        btn.titleLabel?.font = font
        btn.backgroundColor = .white
        btn.setTitleColor(.black, for: .normal)
        btn.addTarget(self, action: #selector(zoomInAction), for: .touchUpInside)
        btn.layer.cornerRadius = 34/2
        return btn
    }()
    
    private var minusBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("➖", for: .normal)
        let font = UIFont.systemFont(ofSize: 18)
        btn.titleLabel?.font = font
        btn.backgroundColor = .white
        btn.setTitleColor(.black, for: .normal)
        btn.addTarget(self, action: #selector(zoomOutAction), for: .touchUpInside)
        btn.layer.cornerRadius = 34/2
        return btn
    }()
    
    private var pinImg: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "pin")
        img.tintColor = .systemRed
        return img
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
        
        setUI()
        setUpMapView()
    }
}

//MARK: - Set UI
extension ContainerMapVC {
    fileprivate
    func setUpMapView() {
        mapView.showsUserLocation = true
        mapView.showsCompass = false
        mapView.showsScale = true
        let initialLocation = CLLocation(latitude: 55.752953, longitude: 37.582065)
        mapView.centerToLocation(initialLocation)
        
        mapView.delegate = self

        currentLocation()
    }
    
    fileprivate
    func currentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.startUpdatingLocation()
    }
    
    fileprivate
    func setUI() {
        self.view.addSubview(titleView)
        self.view.addSubview(mapView)
        self.view.addSubview(titleLbl)
        self.view.addSubview(saveBtn)
        self.view.addSubview(lineBetweenBtns)
        self.view.addSubview(cancelBtn)
        self.view.addSubview(currentLocationBtn)
        self.view.addSubview(plusBtn)
        self.view.addSubview(minusBtn)
        self.view.addSubview(pinImg)
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            mapView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview().offset(10)
                make.height.equalToSuperview().multipliedBy(0.5)
                make.width.equalToSuperview().multipliedBy(0.75)
            }
            
            titleView.snp.makeConstraints { (make) in
                make.bottom.equalTo(mapView.snp.top).offset(50)
                make.height.equalTo(100)
                make.centerX.equalTo(mapView)
                make.width.equalTo(mapView)
            }
            
            titleLbl.snp.makeConstraints { (make) in
                make.top.equalTo(titleView).offset(16)
                make.centerX.equalTo(mapView)
                make.width.equalTo(mapView)
            }
            
            saveBtn.snp.makeConstraints { (make) in
                make.top.equalTo(mapView.snp.bottom).offset(4)
                make.height.equalTo(44)
                make.centerX.equalTo(mapView)
                make.width.equalTo(mapView)
            }
            
            lineBetweenBtns.snp.makeConstraints { (make) in
                make.top.equalTo(saveBtn.snp.bottom)
                make.height.equalTo(1)
                make.centerX.equalTo(mapView)
                make.width.equalTo(mapView)
            }
            
            cancelBtn.snp.makeConstraints { (make) in
                make.top.equalTo(lineBetweenBtns.snp.bottom)
                make.height.equalTo(44)
                make.centerX.equalTo(mapView)
                make.width.equalTo(mapView)
            }
            
            plusBtn.snp.makeConstraints { (make) in
                make.centerY.equalTo(mapView.snp.centerY)
                make.size.width.height.equalTo(34)
                make.right.equalTo(mapView.snp.right).inset(24)
            }
            
            minusBtn.snp.makeConstraints { (make) in
                make.centerX.equalTo(self.plusBtn)
                make.size.equalTo(plusBtn)
                make.bottom.equalTo(plusBtn.snp.top).inset(-10)
            }
            
            currentLocationBtn.snp.makeConstraints { (make) in
                make.top.equalTo(plusBtn.snp.bottom).offset(20)
                make.size.width.height.equalTo(45)
                make.right.equalTo(mapView.snp.right).inset(20)
            }
            
            pinImg.snp.makeConstraints { (make) in
                make.center.equalTo(mapView.snp.center)
                make.size.width.height.equalTo(32)
            }
            
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
}


//MARK: - Actions
extension ContainerMapVC {
    @objc
    fileprivate
    func saveAction() {
        setCenterPosition?(centerPosition)
    }
    
    @objc
    fileprivate
    func cancelAction() {
        handler?()
    }
    
    @objc
    fileprivate
    func zoomInAction(){
        var region: MKCoordinateRegion = mapView.region
        region.span.latitudeDelta /= 2.0
        region.span.longitudeDelta /= 2.0
        mapView.setRegion(region, animated: true)
    }
    
    @objc
    fileprivate
    func zoomOutAction(){
        var region: MKCoordinateRegion = mapView.region
        region.span.latitudeDelta = min(region.span.latitudeDelta * 2.0, 180.0)
        region.span.longitudeDelta = min(region.span.longitudeDelta * 2.0, 180.0)
        mapView.setRegion(region, animated: true)
    }
    
    @objc
    fileprivate
    func tapCurrentLocation(){
        guard let location = locationManager.location else {return}
        updateLocationOnMap(to: location, with: nil)
    }
}


//MARK: - CLLocationManagerDelegate
extension ContainerMapVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        let currentLocation = location.coordinate
        let coordinateRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: 800, longitudinalMeters: 800)
        mapView.setRegion(coordinateRegion, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func updateLocationOnMap(to location: CLLocation, with title: String?) {
        let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
        self.mapView.setRegion(viewRegion, animated: true)
    }
}

//MARK: - MKMapViewDelegate
extension ContainerMapVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        centerPosition =  mapView.centerCoordinate
    }
}
