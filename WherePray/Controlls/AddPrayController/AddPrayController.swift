//
//  AddPrayController.swift
//  WherePray
//
//  Created by dev717 on 28.10.2020.
//

import UIKit
import MapKit

protocol AddPrayProtocol: class {
    func dataPush()
    func showError(message: String)
}

class AddPrayController: UIViewController, UINavigationControllerDelegate {
    private var didSetupConstraints = false
    private var viewModel: AddPrayViewModelProtocol?
    private var errorView: ErrorView?
    private var coordinateFromMV: CLLocationCoordinate2D?
    weak var delegate: UpdateDataDelegate?
    
    let imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.layer.cornerRadius = 5
        imgView.layer.masksToBounds = true
        imgView.clipsToBounds = true
        return imgView
    }()
    
    let titlePrayTF: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Название"
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    let mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.layer.cornerRadius = 5
        mapView.showsCompass = false
        mapView.showsScale = true
        let initialLocation = CLLocation(latitude: 55.752953, longitude: 37.582065)
        mapView.centerToLocation(initialLocation)
        return mapView
    }()
    
    let clearViewMap: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let addPhotoBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Добавить фото", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = btn.titleLabel?.font.withSize(17)
        btn.backgroundColor = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
        btn.addTarget(self, action: #selector(addPhotoAction), for: .touchUpInside)
        btn.layer.cornerRadius = 10
        return btn
    }()
    
    let removeImgBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("×", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = btn.titleLabel?.font.withSize(20)
        btn.backgroundColor = .systemRed
        btn.addTarget(self, action: #selector(removeImgAction), for: .touchUpInside)
        btn.layer.cornerRadius = 25/2
        return btn
    }()
    
    let adressTV: UITextView = {
        let tf = UITextView()
        tf.text = "Адрес"
        tf.textColor = UIColor.lightGray
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor.lightGray.cgColor
        tf.layer.cornerRadius = 5
        return tf
    }()
    
    let descrTV: UITextView = {
        let tf = UITextView()
        tf.text = "Описание"
        tf.textColor = UIColor.lightGray
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor.lightGray.cgColor
        tf.layer.cornerRadius = 5
        return tf
    }()
    
    
    let indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .large
        indicator.hidesWhenStopped = true
        indicator.color = .systemRed
        return indicator
    }()
    
    private var pinImg: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "pin")
        img.tintColor = .systemRed
        return img
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        title = "Добавить место"
        
        self.view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        setNavController()
        setUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyboard()
    }
}

//MARK: - Set UI
extension AddPrayController {
    fileprivate
    func setUI() {
        self.view.addSubview(titlePrayTF)
        self.view.addSubview(mapView)
        self.view.addSubview(clearViewMap)
        self.view.addSubview(addPhotoBtn)
        self.view.addSubview(adressTV)
        self.view.addSubview(descrTV)
        self.view.addSubview(indicator)
        self.view.addSubview(pinImg)
        
        view.setNeedsUpdateConstraints()
        
        adressTV.delegate = self
        descrTV.delegate = self
        
        let tapMapGest = UITapGestureRecognizer(target: self, action: #selector(tapMap))
        clearViewMap.addGestureRecognizer(tapMapGest)
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            titlePrayTF.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(74)
                make.right.left.equalToSuperview().inset(16)
                make.height.equalTo(45)
            }
            
            mapView.snp.makeConstraints { make in
                make.top.equalTo(titlePrayTF.snp.bottom).offset(16)
                make.width.equalTo(titlePrayTF)
                make.centerX.equalTo(titlePrayTF)
                make.height.equalTo(145)
            }
            
            clearViewMap.snp.makeConstraints { make in
                make.size.width.height.equalTo(mapView)
                make.center.equalTo(mapView)
            }
            
            addPhotoBtn.snp.makeConstraints { make in
                make.left.right.equalToSuperview().inset(16)
                make.height.equalTo(45)
                make.top.equalTo(mapView.snp.bottom).offset(20)
            }

            adressTV.snp.makeConstraints { make in
                make.left.right.equalToSuperview().inset(16)
                make.height.equalTo(40)
                make.top.equalTo(addPhotoBtn.snp.bottom).offset(20)
            }
            
            descrTV.snp.makeConstraints { make in
                make.left.right.equalToSuperview().inset(16)
                make.height.equalTo(110)
                make.top.equalTo(adressTV.snp.bottom).offset(20)
            }
            
            indicator.snp.makeConstraints { (make) in
                make.centerX.equalTo(self.view)
                make.centerY.equalTo(self.view)
                make.height.width.equalTo(25)
            }

            pinImg.snp.makeConstraints { (make) in
                make.center.equalTo(mapView.snp.center)
                make.size.width.height.equalTo(32)
            }
            
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    fileprivate
    func setNavController() {
        let buttonItem = UIBarButtonItem(title: "×", style: UIBarButtonItem.Style.plain, target: self, action: #selector(tapClose))
        let font = UIFont.systemFont(ofSize: 32)
        buttonItem.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        
        if self.traitCollection.userInterfaceStyle == .dark {
            buttonItem.tintColor = .white
        } else {
            buttonItem.tintColor = .black
        }
        
        self.navigationItem.leftBarButtonItem = buttonItem
        
        let sendButtonItem = UIBarButtonItem(title: "Отправить", style: UIBarButtonItem.Style.plain, target: self, action: #selector(sendAction))
        let sendFont = UIFont.systemFont(ofSize: 15)
        sendButtonItem.setTitleTextAttributes([NSAttributedString.Key.font: sendFont], for: .normal)
        
        if self.traitCollection.userInterfaceStyle == .dark {
            sendButtonItem.tintColor = .white
        } else {
            sendButtonItem.tintColor = .black
        }
        
        self.navigationItem.rightBarButtonItem = sendButtonItem
    }
    
    fileprivate
    func initImage(image: UIImage) {
        view.addSubview(imgView)
        view.addSubview(removeImgBtn)
        imgView.image = image
        
        self.addPhotoBtn.isHidden = true
        
        UIView.animate(withDuration: 0.5) {
            self.imgView.snp.remakeConstraints { make in
                make.left.right.equalToSuperview().inset(16)
                make.height.equalTo(120)
                make.top.equalTo(self.mapView.snp.bottom).offset(20)
            }
        } completion: { (_) in
            UIView.animate(withDuration: 0.5) {
                self.removeImgBtn.snp.remakeConstraints { make in
                    make.trailing.equalTo(self.imgView.snp.trailing).offset(25/2)
                    make.height.width.equalTo(25)
                    make.top.equalTo(self.imgView.snp.top).offset(-25/2)
                }
            } completion: { (_) in
                UIView.animate(withDuration: 0.5) {
                    self.adressTV.snp.remakeConstraints { make in
                        make.left.right.equalToSuperview().inset(16)
                        make.height.equalTo(40)
                        make.top.equalTo(self.imgView.snp.bottom).offset(20)
                    }
                } completion: { (_) in
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}

//MARK: - Acitons
extension AddPrayController {
    @objc
    fileprivate
    func removeImgAction() {
        UIView.animate(withDuration: 0.5) {
            self.addPhotoBtn.isHidden = false
            
            self.removeImgBtn.snp.updateConstraints { (make) in
                make.height.equalTo(0)
            }
        } completion: { (isComp) in
        }
        
        UIView.animate(withDuration: 0.5) {
            self.imgView.snp.updateConstraints { (make) in
                make.height.equalTo(0)
            }
        } completion: { (isComp) in
        }
        
        UIView.animate(withDuration: 0.5) {
            self.adressTV.snp.remakeConstraints { make in
                make.left.right.equalToSuperview().inset(16)
                make.height.equalTo(40)
                make.top.equalTo(self.addPhotoBtn.snp.bottom).offset(20)
            }
            
        } completion: { (isComp) in
        }
        
        self.imgView.image = nil
        self.view.layoutIfNeeded()
    }
    
    class func create(viewModel: AddPrayViewModelProtocol) -> AddPrayController {
        let currentVC = AddPrayController()
        viewModel.setDependencies(view: currentVC, api: PlacesAPI())
        currentVC.viewModel = viewModel
        return currentVC
    }
    
    fileprivate
    func initiatImgPicker(isCamera: Bool){
        let vc = UIImagePickerController()
        vc.sourceType = isCamera ? .camera : .savedPhotosAlbum
        vc.allowsEditing =  isCamera ? true : false
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @objc
    fileprivate
    func dismissKeyboard() {
        self.view.endEditing(true)

        UIView.animate(withDuration: 0.5) {
            self.titlePrayTF.snp.updateConstraints { (make) in
                make.top.equalToSuperview().offset((74))
            }
            self.view.layoutIfNeeded()
        }
    }
    
    @objc
    fileprivate
    func tapClose() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    fileprivate
    func sendAction() {
        print("send action")
        indicator.startAnimating()
        guard let coordinateFromMV = coordinateFromMV else {
            print("coord is nil")
            showErrorView(message: "coordinate is nil", image: #imageLiteral(resourceName: "noData"))
            return
        }
        
        let prayItem = AddPray(title: self.titlePrayTF.text ?? "", latitude: coordinateFromMV.latitude, longitude: coordinateFromMV.longitude, description: self.descrTV.text ?? "", adress: self.adressTV.text ?? "")
        //viewModel?.dataPush(prayItem: prayItem)
        viewModel?.dataPush(prayItem: prayItem, img: imgView.image)
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//            self.indicator.stopAnimating()
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    @objc
    fileprivate
    func tapMap() {
        print("tap map action")
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        
        containerView.snp.makeConstraints { (make) in
            make.size.equalToSuperview()
        }

        // add child view controller view to container

        let controller = ContainerMapVC()
        addChild(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        controller.handler = {
            containerView.removeFromSuperview()
        }
        
        controller.setCenterPosition = { [weak self] centerPosition in
            
            guard let centerPosition = centerPosition else {return}
            self?.coordinateFromMV = centerPosition
            containerView.removeFromSuperview()
                
            let initialLocation = CLLocation(latitude: centerPosition.latitude, longitude: centerPosition.longitude)
            
            self?.mapView.centerToLocation(initialLocation)
            
            let coordinateRegion = MKCoordinateRegion(center: centerPosition, latitudinalMeters: 800, longitudinalMeters: 800)
            self?.mapView.setRegion(coordinateRegion, animated: true)
        }

        controller.view.snp.makeConstraints { (make) in
            make.size.equalToSuperview()
        }

        controller.didMove(toParent: self)
        
        dismissKeyboard()
    }
    
    @objc
    fileprivate
    func addPhotoAction() {
        print("tap add photo action")
        dismissKeyboard()
     
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let firstAction: UIAlertAction = UIAlertAction(title: "Галерея", style: .default) { action -> Void in
            self.initiatImgPicker(isCamera: false)
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "Камера", style: .default) { action -> Void in
            self.initiatImgPicker(isCamera: true)
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Отмена", style: .cancel) { action -> Void in }

        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)

        present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
    }
}

extension AddPrayController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            
            UIView.animate(withDuration: 0.5) {
                self.titlePrayTF.snp.updateConstraints { (make) in
                    make.top.equalToSuperview().offset(-80)
                }
                self.view.layoutIfNeeded()
            }

        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textView == adressTV ? "Адрес" : "Описание"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension AddPrayController:UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if let pickedImage = info[.originalImage] as? UIImage {
            print(pickedImage.size)
            initImage(image: pickedImage)
        } else if let pickedImage = info[.editedImage] as? UIImage {
            print(pickedImage.size)
            initImage(image: pickedImage)
        } else {
            print("No image found")
        }
    }
}

extension AddPrayController: AddPrayProtocol {
    func dataPush() {
        hideErrorView()
        self.indicator.stopAnimating()
        self.dismiss(animated: true, completion: {
            self.delegate?.updateData()
        })
    }
    
    func showError(message: String) {
        showErrorView(message: message, image: UIImage(named: "errorIcon")!)
    }

    private func showErrorView(message: String, image: UIImage) {
        hideErrorView()
        errorView = ErrorView(frame: self.view.frame)
        errorView?.updateWith(image: image, text: message)
        errorView?.closeHandler = {
            self.hideErrorView()
        }
        errorView?.alpha = 0
        self.view.addSubview(errorView!)
        UIView.animate(withDuration: 0.2) { [weak errorView] in
            errorView?.alpha = 1
        }
    }
    
    private func hideErrorView() {
        self.indicator.stopAnimating()
        if let errorView = errorView {
            UIView.animate(withDuration: 0.2, animations: { [weak errorView] in
                errorView?.alpha = 0
            }) { [weak errorView] (_) in
                errorView?.removeFromSuperview()
            }
        }
    }
}
