//
//  AddPrayViewModel.swift
//  WherePray
//
//  Created by Kerim Khasbulatov on 22.11.2020.
//

import UIKit

protocol AddPrayViewModelProtocol {
    func setDependencies(view: AddPrayProtocol?, api: PlacesAPI?)
    func dataPush(prayItem: AddPray, img: UIImage?)
}

class AddPrayViewModel {
    //MARK:- Properties
    private weak var view: AddPrayProtocol?
    private var api: PlacesAPI?
}

extension AddPrayViewModel: AddPrayViewModelProtocol {
    func setDependencies(view: AddPrayProtocol?, api: PlacesAPI?) {
        self.view = view
        self.api = api
    }
    
    func dataPush(prayItem: AddPray, img: UIImage?) {
        
        if let img = img {
            api?.addPray(prayItem: prayItem, and: img, completion: { [weak self] (result) in
                switch result {
                case .success(_):
                    self?.view?.dataPush()
                case .failure(let error):
                    print(error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
                    self?.view?.showError(message: error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
                }
            })
        } else {
            api?.sendPlace(prayItem: prayItem, completion: { [weak self] (result) in
                switch result {
                case .success(_):
                    self?.view?.dataPush()
                case .failure(let error):
                    print(error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
                    self?.view?.showError(message: error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
                }
            })
        }
    }
}
