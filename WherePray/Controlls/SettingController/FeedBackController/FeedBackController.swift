//
//  FeedBackController.swift
//  WherePray
//
//  Created by Kerim Khasbulatov on 03.01.2021.
//

import UIKit

class FeedBackController: UIViewController {
    private var didSetupConstraints = false
        
    var tgBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = #colorLiteral(red: 0.3019607843, green: 0.7568627451, blue: 0.9137254902, alpha: 1)
        btn.setTitle("Чат в Телеграм", for: .normal)
        btn.layer.cornerRadius = 5
        btn.addTarget(self, action: #selector(feedbackAction), for: .touchUpInside)
        return btn
    }()
    
    var instBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.2666666667, blue: 0.3254901961, alpha: 1)
        btn.setTitle("Мы в инстаграм", for: .normal)
        btn.layer.cornerRadius = 5
        btn.addTarget(self, action: #selector(feedbackAction), for: .touchUpInside)
        return btn
    }()
    
    var mailBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = #colorLiteral(red: 0.3960784314, green: 0.4274509804, blue: 0.4705882353, alpha: 1)
        btn.setTitle("support@prayway.ru", for: .normal)
        btn.layer.cornerRadius = 5
        btn.addTarget(self, action: #selector(feedbackAction), for: .touchUpInside)
        return btn
    }()
 
    let timeLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textColor = .darkGray
        lbl.text = "Время работы 09:00 - 00:00"
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                        
        self.view.backgroundColor = .white
        
        title = "Служба поддержки"
        tabBarItem.title = " "
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(actionBack))

        setUI()
    }
}

//MARK: - Set UI
extension FeedBackController {
    fileprivate
    func setUI() {
        view.addSubview(tgBtn)
        view.addSubview(instBtn)
        view.addSubview(mailBtn)
        view.addSubview(timeLbl)
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            
            tgBtn.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(0.8)
                make.width.lessThanOrEqualTo(300)
                make.height.equalTo(45)
                make.bottom.equalToSuperview().multipliedBy(0.88)
            }
            
            instBtn.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.width.height.equalTo(tgBtn)
                make.bottom.equalTo(tgBtn.snp.top).inset(-10)
            }
            
            mailBtn.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.width.height.equalTo(tgBtn)
                make.bottom.equalTo(instBtn.snp.top).inset(-10)
            }
            
            timeLbl.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.bottom.equalTo(mailBtn.snp.top).inset(-32)
            }
            
          
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
}

//MARK: - Set Actions
extension FeedBackController {
    @objc
    fileprivate
    func feedbackAction() {
        print(#function)
    }
    
    @objc
    fileprivate
    func actionBack() {
        navigationController?.popViewController(animated: true)
    }
}

