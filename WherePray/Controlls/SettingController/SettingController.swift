//
//  SettingController.swift
//  WherePray
//
//  Created by Kerim Khasbulatov on 03.01.2021.
//

import UIKit

class SettingController: UIViewController {
    private var didSetupConstraints = false
        
    var feedbackBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5529411765, blue: 0.9254901961, alpha: 1)
        btn.setTitle("Служба поддержки", for: .normal)
        btn.layer.cornerRadius = 5
        btn.addTarget(self, action: #selector(feedbackAction), for: .touchUpInside)
        return btn
    }()
    
    //DetailView
    let imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.clipsToBounds = true
        imgView.image = UIImage(named: "pray")
        imgView.contentMode = .scaleAspectFill
        imgView.layer.cornerRadius = 4
        return imgView
    }()
    
    let headerLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 1
        lbl.font = UIFont.boldSystemFont(ofSize: 32)
        lbl.textColor = .black
        lbl.text = "Pray Way"
        return lbl
    }()
    
    let versionLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textColor = .lightGray
        lbl.text = "Версия 1.5.32 (321)"
        return lbl
    }()
    
    let descrLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 4
        lbl.font = UIFont.systemFont(ofSize: 17)
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.text = "Удобный поиск молебной по всему миру, проще как ни когда"
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                        
        self.view.backgroundColor = .white
        
        title = "О приложении"
        tabBarItem.title = " "
        setUI()
    }
}

//MARK: - Set UI
extension SettingController {
    fileprivate
    func setUI() {
        view.addSubview(feedbackBtn)
        view.addSubview(imgView)
        view.addSubview(headerLbl)
        view.addSubview(versionLbl)
        view.addSubview(descrLbl)
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            
            imgView.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.height.width.equalTo(160)
                make.top.equalToSuperview().offset(106)
            }
           
            headerLbl.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalTo(imgView.snp.bottom).offset(14)
            }
            
            versionLbl.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalTo(headerLbl.snp.bottom).offset(4)
            }
            
            descrLbl.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.left.right.equalToSuperview().inset(32)
                make.top.equalTo(versionLbl.snp.bottom).offset(40)
            }
            
            feedbackBtn.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(0.8)
                make.width.lessThanOrEqualTo(300)
                make.height.equalTo(45)
                make.bottom.equalToSuperview().multipliedBy(0.88)
            }
            
          
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
}

//MARK: - Set Actions
extension SettingController {
    @objc
    fileprivate
    func feedbackAction() {
        navigationController?.pushViewController(FeedBackController(), animated: true)
    }
}
