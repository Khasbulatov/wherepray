//
//  ViewController.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import UIKit
import MapKit
import CoreLocation
import SnapKit

protocol UpdateDataDelegate: class {
    func updateData()
}

protocol MapVCProtocol: class {
    func dataFetched()
    func showError(message: String)
}

class MapViewController: UIViewController {
    
    //MARK: - Variable
    private var mapView = MKMapView()
    weak var delegate: HomeControllerDelegate?
    
    private var didSetupConstraints = false
    
    fileprivate let locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    private var currentLocationBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(named: "userLocation"), for: .normal)
        btn.addTarget(self, action: #selector(tapCurrentLocation), for: .touchUpInside)
        return btn
    }()
    
    private var addPrayBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(named: "plusBtn"), for: .normal)
        btn.addTarget(self, action: #selector(addPrayAction), for: .touchUpInside)
        return btn
    }()
    
    private var plusBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("➕", for: .normal)
        let font = UIFont.systemFont(ofSize: 18)
        btn.titleLabel?.font = font
        btn.backgroundColor = .white
        btn.setTitleColor(.black, for: .normal)
        btn.addTarget(self, action: #selector(zoomInAction), for: .touchUpInside)
        btn.layer.cornerRadius = 34/2
        return btn
    }()
    
    private var minusBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("➖", for: .normal)
        let font = UIFont.systemFont(ofSize: 18)
        btn.titleLabel?.font = font
        btn.backgroundColor = .white
        btn.setTitleColor(.black, for: .normal)
        btn.addTarget(self, action: #selector(zoomOutAction), for: .touchUpInside)
        btn.layer.cornerRadius = 34/2
        return btn
    }()
    
    //TODO: MenuBtn
//    private var menuBtn: UIButton = {
//        let btn = UIButton()
//        btn.setTitle("", for: .normal)
//        btn.setImage(UIImage(named: "menu"), for: .normal)
//        btn.addTarget(self, action: #selector(handleMenuToggle), for: .touchUpInside)
//        return btn
//    }()
    
    private var loadingData = true
    private var viewModel: MapVCViewModelProtocol?
    private var errorView: ErrorView?

    private var prayItems: [PrayItems] = []
    
    enum CardState {
        case expanded
        case collapsed
    }
    
    var cardViewController:CardViewController!
    var visualEffectView:UIVisualEffectView!
    
    var cardHeight:CGFloat = 0
    let cardHandleAreaHeight:CGFloat = 225
    
    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    
    fileprivate var runningAnimations = [UIViewPropertyAnimator]()
    fileprivate var animationProgressWhenInterrupted:CGFloat = 0
    fileprivate var selectedAnnotation: PrayItems?
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel?.fetchPlaces(isFirstLoad: true)
        
        setUI()
        setUpMapView()
    }
}

//MARK: - Set UI
extension MapViewController {
    fileprivate
    func setUI() {
        self.view.addSubview(mapView)
        self.view.addSubview(currentLocationBtn)
        self.view.addSubview(addPrayBtn)
        //TODO: MenuBtn
        //self.view.addSubview(menuBtn)
        self.view.addSubview(plusBtn)
        self.view.addSubview(minusBtn)
        
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            mapView.snp.makeConstraints { make in
                make.size.equalToSuperview()
            }
            
            plusBtn.snp.makeConstraints { (make) in
                make.centerY.equalTo(self.view)
                make.size.width.height.equalTo(34)
                make.right.equalTo(view.snp.right).inset(24)
            }
            
            minusBtn.snp.makeConstraints { (make) in
                make.centerX.equalTo(self.plusBtn)
                make.size.equalTo(plusBtn)
                make.bottom.equalTo(plusBtn.snp.top).inset(-10)
            }
            
            currentLocationBtn.snp.makeConstraints { (make) in
                make.top.equalTo(plusBtn.snp.bottom).offset(20)
                make.size.width.height.equalTo(45)
                make.right.equalTo(view.snp.right).inset(20)
            }
            
            addPrayBtn.snp.makeConstraints { (make) in
                make.size.width.height.equalTo(60)
                make.top.equalTo(view.snp.top).offset(40)
                make.centerX.equalTo(plusBtn)
            }
  
            //TODO: MenuBtn
//            menuBtn.snp.makeConstraints { (make) in
//                make.size.width.height.equalTo(45)
//                make.left.equalTo(view.snp.left).offset(20)
//                make.top.equalTo(view.snp.top).offset(40)
//            }

            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
}

//MARK: - FUNC
extension MapViewController {
    fileprivate
    func setData() {
        guard let prayItemsLoc = viewModel?.locations else {
            print("locaitons is nill")
            return
        }

        for prayItem in prayItemsLoc {
            guard let _ = prayItem.latitude, let _ = prayItem.longitude else {
                print("latituede or longitude is nil")
                return
            }
            let prayItemLoc = PrayItems()
            prayItemLoc.adress = prayItem.adress ?? ""
            prayItemLoc.title = prayItem.title
            prayItemLoc.adress = prayItem.adress ?? ""
            prayItemLoc.img = prayItem.imageLinks?.first
            prayItemLoc.header = prayItem.title ?? ""
            prayItemLoc.coordinate = CLLocationCoordinate2D(latitude: prayItem.latitude!, longitude: prayItem.longitude!)
            self.prayItems.append(prayItemLoc)
        }

        mapView.addAnnotations(prayItems)
    }
    
    fileprivate
    func setUpMapView() {
        mapView.showsUserLocation = true
        mapView.showsCompass = false
        mapView.showsScale = true
        //izb
        //42,564051
        //47,867522
        let initialLocation = CLLocation(latitude: 42.564051, longitude: 47.867522)
        mapView.centerToLocation(initialLocation)
        
        mapView.delegate = self

        currentLocation()
    }
    
    fileprivate
    func currentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.startUpdatingLocation()
    }
    
    class func create(viewModel: MapVCViewModelProtocol) -> MapViewController {
        let currentVC = MapViewController()
        viewModel.setDependencies(view: currentVC, api: PlacesAPI())
        currentVC.viewModel = viewModel
        return currentVC
    }
}

//MARK: - ACTIONS
extension MapViewController {
    @objc
    fileprivate
    func handleMenuToggle(){
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    @objc
    fileprivate
    func zoomInAction(){
        var region: MKCoordinateRegion = mapView.region
        region.span.latitudeDelta /= 2.0
        region.span.longitudeDelta /= 2.0
        mapView.setRegion(region, animated: true)
    }
    
    @objc
    fileprivate
    func zoomOutAction(){
        var region: MKCoordinateRegion = mapView.region
        region.span.latitudeDelta = min(region.span.latitudeDelta * 2.0, 180.0)
        region.span.longitudeDelta = min(region.span.longitudeDelta * 2.0, 180.0)
        mapView.setRegion(region, animated: true)
    }
    
    @objc
    fileprivate
    func tapCurrentLocation(){
        guard let location = locationManager.location else {return}
        updateLocationOnMap(to: location, with: nil)
    }
    
    @objc
    fileprivate
    func addPrayAction(){
        print("add pray location")
        let vc = AddPrayController.create(viewModel: AddPrayViewModel())
        vc.delegate = self
        let nc = UINavigationController(rootViewController: vc)
        self.present(nc, animated: true, completion: nil)
    }
}

//MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        let currentLocation = location.coordinate
        let coordinateRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: 800, longitudinalMeters: 800)
        mapView.setRegion(coordinateRegion, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func updateLocationOnMap(to location: CLLocation, with title: String?) {
        let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
        self.mapView.setRegion(viewRegion, animated: true)
    }
}

//MARK: - MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        selectedAnnotation = view.annotation as? PrayItems
        setupCard()
    }
}

//MARK: - Add Card View
extension MapViewController {
    func setupCard() {
        self.cardHeight = self.view.bounds.height * 0.6
        
        visualEffectView = UIVisualEffectView()
        visualEffectView.frame = self.view.frame
        self.view.addSubview(visualEffectView)
        
        cardViewController = CardViewController()
        self.addChild(cardViewController)
        self.view.addSubview(cardViewController.view)
        
        if let prayItem = selectedAnnotation {
            cardViewController.configure(prayItem: prayItem)
        }
        
        cardViewController.handler = {
            self.cardViewController.willMove(toParent: nil)
            self.cardViewController.view.removeFromSuperview()
            self.cardViewController.removeFromParent()
            self.visualEffectView.removeFromSuperview()
            self.runningAnimations.removeAll()
            self.cardVisible = false
            self.mapView.deselectAnnotation(self.selectedAnnotation, animated: true)
        }
        
        self.cardViewController.view.frame = CGRect(x: 0, y: self.view.frame.height - self.cardHandleAreaHeight, width: self.view.bounds.width, height: cardHeight)
        
        self.cardViewController.view.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.cardViewController.view.alpha = 1
        }

        cardViewController.view.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleCardTap(recognzier:)))
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleCardPan(recognizer:)))
        
        cardViewController.handleArea.addGestureRecognizer(tapGestureRecognizer)
        cardViewController.handleArea.addGestureRecognizer(panGestureRecognizer)
    }

    @objc
    func handleCardTap(recognzier:UITapGestureRecognizer) {
        switch recognzier.state {
        case .ended:
            animateTransitionIfNeeded(state: nextState, duration: 0.9)
        default:
            break
        }
    }
    
    @objc
    func handleCardPan (recognizer:UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            startInteractiveTransition(state: nextState, duration: 0.9)
        case .changed:
            let translation = recognizer.translation(in: self.cardViewController.handleArea)
            var fractionComplete = translation.y / cardHeight
            fractionComplete = cardVisible ? fractionComplete : -fractionComplete
            updateInteractiveTransition(fractionCompleted: fractionComplete)
        case .ended:
            continueInteractiveTransition()
        default:
            break
        }
    }
    
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.cardHeight
                case .collapsed:
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
                }
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
            
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    self.cardViewController.view.layer.cornerRadius = 12
                case .collapsed:
                    self.cardViewController.view.layer.cornerRadius = 0
                }
            }

            cornerRadiusAnimator.startAnimation()
            runningAnimations.append(cornerRadiusAnimator)
            
            let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.visualEffectView.effect = UIBlurEffect(style: .dark)
                case .collapsed:
                    self.visualEffectView.effect = nil
                }
            }
            
            blurAnimator.startAnimation()
            runningAnimations.append(blurAnimator)
        }
    }
    
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        for animator in runningAnimations {
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        for animator in runningAnimations {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    func continueInteractiveTransition (){
        for animator in runningAnimations {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
}

//MARK: - MapVCProtocol
extension MapViewController: MapVCProtocol {
    func dataFetched() {
        hideErrorView()
        loadingData = false
        if viewModel?.placesCount == 0 {
            showErrorView(message: "No Results !\nTry moving around", image: UIImage(named: "noData")!)
        }
        setData()
    }
    
    func showError(message: String) {
        showErrorView(message: message, image: UIImage(named: "errorIcon")!)
    }

    private func showErrorView(message: String, image: UIImage) {
        hideErrorView()
        errorView = ErrorView(frame: self.view.frame)
        errorView?.updateWith(image: image, text: message)
        errorView?.alpha = 0
        self.view.addSubview(errorView!)
        UIView.animate(withDuration: 0.2) { [weak errorView] in
            errorView?.alpha = 1
        }
    }
    
    private func hideErrorView() {
        if let errorView = errorView {
            UIView.animate(withDuration: 0.2, animations: { [weak errorView] in
                errorView?.alpha = 0
            }) { [weak errorView] (_) in
                errorView?.removeFromSuperview()
            }
        }
    }
}

extension MapViewController: UpdateDataDelegate {
    func updateData() {
        viewModel?.fetchPlaces(isFirstLoad: true)
    }
}
