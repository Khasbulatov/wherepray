//
//  MapVCViewModel.swift
//  WherePray
//
//  Created by dev717 on 15.11.2020.
//

import Foundation

protocol MapVCViewModelProtocol {
    var placesCount: Int? { get }
    var locations: [PrayModel]? { get }
    func setDependencies(view: MapVCProtocol?, api: PlacesAPI?)
    func fetchPlaces(isFirstLoad: Bool)
    func place(for index: Int) -> PrayModel?
}

class MapVCViewModel {
    //MARK:- Properties
    private weak var view: MapVCProtocol?
    private var api: PlacesAPI?
    private var places: [PrayModel] = []
}

extension MapVCViewModel: MapVCViewModelProtocol {
    var placesCount: Int? {
        return places.count
    }
    
    var locations: [PrayModel]? {
        return places
    }
    
    func setDependencies(view: MapVCProtocol?, api: PlacesAPI?) {
        self.view = view
        self.api = api
    }
    
    func fetchPlaces(isFirstLoad: Bool) {
        api?.getPlaces(skip: 0, completion: { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.places = response?.places ?? []
                self?.view?.dataFetched()
            case .failure(let error):
                print(error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
                self?.view?.showError(message: error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
            }
        })
    }
    
    func place(for index: Int) -> PrayModel? {
        return places[index]
    }
}
