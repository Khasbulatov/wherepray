//
//  PrayListViewModel.swift
//  WherePray
//
//  Created by dev717 on 08.11.2020.
//

import Foundation

protocol PrayListViewModelProtocol {
    var numberOfRows: Int? { get }
    func setDependencies(view: PrayListProtocol?, api: PlacesAPI?)
    func fetchPlaces(isFirstLoad: Bool)
    func place(for index: Int) -> PrayModel?
}

class PrayListViewModel {
    //MARK:- Properties
    private weak var view: PrayListProtocol?
    private var api: PlacesAPI?
    private var places: [PrayModel] = []
    private var page = 0
    private var totalItems = 0
    private let semaphore = DispatchSemaphore(value: 1)
}

extension PrayListViewModel: PrayListViewModelProtocol {
    
    var numberOfRows: Int? {
        return places.count
    }
    
    func setDependencies(view: PrayListProtocol?, api: PlacesAPI?) {
        self.view = view
        self.api = api
    }
    
    func fetchPlaces(isFirstLoad: Bool) {
        if !isFirstLoad && places.count >= totalItems { return }
        if isFirstLoad {
            places.removeAll()
            page = 0
            totalItems = 0
        }
        api?.getPlaces(skip: page*20, completion: { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.page += 1
                self?.totalItems = response?.totalResults ?? 0
                self?.places = response?.places ?? []
                self?.view?.dataFetched()
            case .failure(let error):
                print(error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
                self?.view?.showError(message: error.userInfo[NSLocalizedDescriptionKey] as? String ?? "")
            }
        })
    }
    
    func place(for index: Int) -> PrayModel? {
        return places[index]
    }
}
