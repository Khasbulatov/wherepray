//
//  PrayListCell.swift
//  WherePray
//
//  Created by dev717 on 25.10.2020.
//

import UIKit
import SDWebImage

class PrayListCell: UITableViewCell {
    
    let imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 4
        return imgView
    }()
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.systemFont(ofSize: 13)
        return lbl
    }()
    
    let adressLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .gray
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let openCloseLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .systemRed
        return lbl
    }()
    
    let bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add the UI components
        contentView.addSubview(imgView)
        contentView.addSubview(titleLbl)
        contentView.addSubview(adressLbl)
        contentView.addSubview(openCloseLbl)
        contentView.addSubview(bottomLine)
        
        imgView.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView)
            make.top.bottom.left.equalToSuperview().inset(20)
            make.height.equalTo(65)
            make.width.equalTo(75)
        }
        
        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(imgView.snp.top).offset(-2)
            make.left.equalTo(imgView.snp.right).offset(16)
            make.right.equalToSuperview().inset(20)
        }
        
        adressLbl.snp.makeConstraints { (make) in
            make.top.equalTo(titleLbl.snp.bottom).offset(8)
            make.left.equalTo(titleLbl.snp.left)
            make.width.equalTo(titleLbl.snp.width)
        }
        
        openCloseLbl.snp.makeConstraints { (make) in
            make.top.equalTo(adressLbl.snp.bottom).offset(8)
            make.left.equalTo(titleLbl.snp.left)
            make.width.equalTo(titleLbl.snp.width)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(20)
            make.height.equalTo(1)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(prayItem: PrayItems) {
        titleLbl.text = prayItem.header
        adressLbl.text = prayItem.adress
        if let img = prayItem.img {
            imgView.image = UIImage(named: img)
        }
        if prayItem.isOpen {
            openCloseLbl.text = "Открыто"
            openCloseLbl.textColor = .systemGreen
        } else {
            openCloseLbl.text = "Закрыто"
            openCloseLbl.textColor = .systemRed
        }
    }
    
    func configure(prayItem: PrayModel) {
        titleLbl.text = prayItem.title
        adressLbl.text = prayItem.adress
        imgView.sd_setImage(with: URL(string: prayItem.imageLinks?.first ?? ""),
                            placeholderImage: UIImage(named: "2"),
                            options: [])
    }
}

