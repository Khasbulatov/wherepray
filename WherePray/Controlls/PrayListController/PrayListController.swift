//
//  PrayListController.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import UIKit

protocol PrayListProtocol: class {
    func dataFetched()
    func showError(message: String)
}

class PrayListController: UIViewController {
    private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
        
        tableView.register(PrayListCell.self, forCellReuseIdentifier: "PrayListCell")
        
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private var prayItems: [PrayItems] = []
    weak var delegate: HomeControllerDelegate?
    private var viewModel: PrayListViewModelProtocol?
    private var loadingData = true
    private var errorView: ErrorView?
    private var shownIndexes : [IndexPath] = [] // used for cells animation

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.loadingData = true
        self.viewModel?.fetchPlaces(isFirstLoad: true)

        tableView.startLoading()

        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

//MARK: - SET UI
extension PrayListController {
    fileprivate
    func configureUI() {
        tableView.frame = self.view.frame
        
        self.view.addSubview(tableView)
        
        self.prayItems = PrayItems.loadJson() ?? []
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.backgroundColor = .white
        
        setNavController()
    }
    
    fileprivate func setNavController() {
        //TODO: Tabbar
//        let buttonItem = UIBarButtonItem(title: "×", style: UIBarButtonItem.Style.plain, target: self, action: #selector(tapMenu))
//        let font = UIFont.systemFont(ofSize: 40)
//        buttonItem.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
//
//        if self.traitCollection.userInterfaceStyle == .dark {
//            buttonItem.tintColor = .white
//        } else {
//            buttonItem.tintColor = .black
//        }
//
//        self.navigationItem.leftBarButtonItem = buttonItem
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Молебные"
        if self.traitCollection.userInterfaceStyle != .dark {
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        }
    }
}

//MARK: - Methods
extension PrayListController {
    @objc
    fileprivate
    func tapMenu() {
        dismiss(animated: true, completion: nil)
    }
    
    class func create(viewModel: PrayListViewModelProtocol) -> PrayListController {
        let currentVC = PrayListController()
        viewModel.setDependencies(view: currentVC, api: PlacesAPI())
        currentVC.viewModel = viewModel
        return currentVC
    }
}

//MARK: - UITableViewDelegate
extension PrayListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PrayListCell", for: indexPath) as? PrayListCell, let prayItem = viewModel?.place(for: indexPath.row)  else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.configure(prayItem: prayItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let prayItem = viewModel?.place(for: indexPath.row) else {return}
                
        var indexPathLoc = indexPath
        indexPathLoc.row = indexPath.row + 1
        let cardViewController = CardViewController(startFrame: tableView.cellForRow(at: indexPath)?.frame)
        cardViewController.configure(prayItem: prayItem)
        cardViewController.modalPresentationStyle = .fullScreen
        self.present(cardViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = (viewModel?.numberOfRows ?? 0) - 1
        if !loadingData && indexPath.row == lastElement {
                loadingData = true
                self.viewModel?.fetchPlaces(isFirstLoad: false)
        }
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            cell.transform = CGAffineTransform(translationX: 0, y: 55)
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 10, height: 10)
            cell.alpha = 0
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            UIView.commitAnimations()
        }
    }
}

//MARK: - PrayListProtocol
extension PrayListController: PrayListProtocol {
    func dataFetched() {
        hideErrorView()
        loadingData = false
        tableView.stopLoading()
        if viewModel?.numberOfRows == 0 {
            showErrorView(message: "No Results !\nTry moving around", image: UIImage(named: "noData")!)
        }
        tableView.reloadData()
    }
    
    func showError(message: String) {
        showErrorView(message: message, image: UIImage(named: "errorIcon")!)
    }

    private func showErrorView(message: String, image: UIImage) {
        hideErrorView()
        errorView = ErrorView(frame: tableView.frame)
        errorView?.updateWith(image: image, text: message)
        errorView?.alpha = 0
        self.view.addSubview(errorView!)
        UIView.animate(withDuration: 0.2) { [weak errorView] in
            errorView?.alpha = 1
        }
    }
    
    private func hideErrorView() {
        if let errorView = errorView {
            UIView.animate(withDuration: 0.2, animations: { [weak errorView] in
                errorView?.alpha = 0
            }) { [weak errorView] (_) in
                errorView?.removeFromSuperview()
            }
        }
    }
}

