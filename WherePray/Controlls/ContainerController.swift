//
//  ContainerController.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import UIKit

class ContainerController: UIViewController {
    
    var menuController: MenuController!
    var centerController: UIViewController!
    var isExpanded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureHomeController()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        isExpanded
    }
}

extension ContainerController {
    fileprivate
    func configureHomeController() {
        let mapVC = MapViewController.create(viewModel: MapVCViewModel())
        mapVC.modalPresentationStyle = .fullScreen
        
        centerController = mapVC
        (centerController as! MapViewController).delegate = self
        
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    fileprivate
    func configureMenuController() {
        if menuController == nil {
            menuController = MenuController()
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }
    
    fileprivate
    func animatePanel(shouldExpand: Bool, menuOption: MenuItem?) {
        if shouldExpand {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                self.centerController.view.frame.origin.x = 0
            }) {(_) in
                guard let menuOption = menuOption else {return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
        }
        
        animateStatusBar()
    }
    
    fileprivate
    func didSelectMenuOption(menuOption: MenuItem) {
        switch menuOption.type {
        case .logo:
            print("logo")
        case .prayList:
            let prayVC = PrayListController.create(viewModel: PrayListViewModel())
            prayVC.delegate = self
            let prayNC = UINavigationController(rootViewController: prayVC)
            prayNC.modalPresentationStyle = .fullScreen
            present(prayNC, animated: true, completion: nil)
        case .infoApp:
            print("infoApp")
        case .rate:
            print("rate")
        case .callBack:
            print("callBack")
        }
    }
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
}

extension ContainerController: HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuItem?) {
        if !isExpanded {
            configureMenuController()
        }
        
        isExpanded = !isExpanded
        animatePanel(shouldExpand: isExpanded, menuOption: menuOption)
    }
}
