//
//  ListMenuCell.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import UIKit

class ListMenuCell: UITableViewCell {
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 20)
        return lbl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add the UI components
        contentView.addSubview(titleLbl)
        
        titleLbl.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView)
            make.bottom.top.leading.trailing.equalToSuperview().inset(20)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(menuItem: MenuItem) {
        titleLbl.text = menuItem.title
    }
}
