//
//  MenuController.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import UIKit

class MenuController: UIViewController {
    private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        tableView.register(ListMenuCell.self, forCellReuseIdentifier: "ListMenuCell")
        tableView.register(LogoMenuCell.self, forCellReuseIdentifier: "LogoMenuCell")
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private var menuItems = [
        MenuItem(type: .logo, img: "pray"),
        MenuItem(type: .prayList, title: "Молебные"),
        MenuItem(type: .infoApp, title: "О приложении"),
        MenuItem(type: .rate, title: "Оценить"),
        MenuItem(type: .callBack, title: "Связаться с нами"),
    ]
    
    weak var delegate: HomeControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.frame = self.view.frame
        self.view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
}

//MARK: - UITableViewDelegate
extension MenuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuItem = menuItems[indexPath.row]
        if menuItem.type == .logo {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LogoMenuCell", for: indexPath) as? LogoMenuCell else {
                return UITableViewCell()
            }
            cell.configure(imgString: menuItem.img ?? "")
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListMenuCell", for: indexPath) as? ListMenuCell else {
                return UITableViewCell()
            }
            cell.configure(menuItem: menuItem)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menuItems[indexPath.row]
        
        delegate?.handleMenuToggle(forMenuOption: menuItem)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if menuItems[indexPath.row].type == .logo {
            return 140
        }
        return UITableView.automaticDimension
    }
}
