//
//  LogoMenuCell.swift
//  WherePray
//
//  Created by dev717 on 24.10.2020.
//

import UIKit

class LogoMenuCell: UITableViewCell {
    let img = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add the UI components
        contentView.addSubview(img)
        
        img.snp.makeConstraints { (make) in
            make.height.width.equalTo(50)
            make.centerY.equalTo(contentView)
            make.left.equalTo((self.contentView.frame.width - 80)/2)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(imgString: String) {
        img.image = UIImage(named: imgString)
    }
}

